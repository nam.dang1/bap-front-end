function openNav() {
    $('#sidebar').css({ "width": "300px" });
    $('.bar').css({ "width": "100%", "transition": "0.5s" });
    $("#menu").css({ "left": "300px", "transition": "0.5s" });


}

function closeNav() {
    document.getElementById("sidebar").style.width = "0";
    document.getElementById("menu").style.left = "0";
    $("#body").css({ "margin-left": "0" });
    $('.bar').css({ "width": "0" });
    document.body.style.backgroundColor = "white";
}

function openSearch() {
    $("#search").css({ "height": "100%", "transition": "0.5s" });
    $("#search textarea").css({ "display": "block", "transition": "0.5s" });
}

function closeSearch() {
    $("#search").css({ "height": "0", "transition": "0.5s" });
    $("#search textarea").css({ "transition": "0.5s" });
}

$(window).on('scroll', () => {
    let screen = $(window).width();
    if (screen > 1280) {
        let scrollTop = $(window).scrollTop();
        if (scrollTop > 200) {
            $('.logo img').stop().animate({ height: "120px", width: "170px" }, 300);
        } else if (scrollTop < 200) {
            $('.logo img').stop().animate({ height: "170px", width: "400px" }, 300);
        }
    }

});